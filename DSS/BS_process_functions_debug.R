#'readBScoverage
#'
#'Fetch Bismark coverage files from the metadata table
#'When fetched, run mergeDuplicateInDesign if replicate = TRUE
#'
#'
#'@param BSmetadata path to the experimental metadata/design file following template
#'@param BSpath path to the directory containing the Bismark coverage files
#'@param replicate boolean spicifying if there are technical replicate listed in the experimental design file
#'
#'@export
#'
#'@return study_DT, a dataTable that binds all coverage files by their labels
readBScoverage <- function(BSmetadata, pathtofiles, replicate = FALSE){
  
  #Make list of DT
  study_DT <- lapply(1:nrow(BSmetadata), function(x){
    
    bedcoverageLabel          <- as.character(BSmetadata[x,1])
    bedcoverageFile           <- fread(file = paste(pathTofiles, BSmetadata[x,4], sep=""), stringsAsFactors=FALSE)
    colnames(bedcoverageFile) <- c("chr", "start", "stop", "Fraction", "Cyt", "Thy")
    bedcoverageFile$label     <- rep(bedcoverageLabel, nrow(bedcoverageFile))
    
    return(bedcoverageFile)
    
  })
  
  #Bind list of DT
  study_DT    <- do.call(rbind, study_DT)
  
  
  if(replicate){
    
    #Check for replicate
    merged_DT <- mergeDuplicateInDesign(BSmetadata, study_DT)
    
  }else{
    
    return(study_DT)
    
  }  
}

#'readBScoverage_stranded
#'
#'Fetch Bismark methylation calls stranded files from the metadata table
#'When fetched, run mergeDuplicateInDesign if replicate = TRUE
#'
#'
#'@param BSmetadata path to the experimental metadata/design file following template
#'@param BSpath path to the directory containing the Bismark coverage files
#'@param replicate boolean spicifying if there are technical replicate listed in the experimental design file
#'
#'@export
#'
#'@return study_DT, a dataTable that binds all coverage files by their labels
readBScoverage_stranded <- function(BSmetadata, pathtofiles, replicate = FALSE){
  
  #Make list of DT
  study_DT <- lapply(1:nrow(BSmetadata), function(x){
    
    bedcoverageLabel          <- as.character(BSmetadata[x,1])
    #bedcoverageFile           <- suppressMessages(fread(file = file.path(pathtofiles, BSmetadata[x,4])))
    bedcoverageFile           <- suppressMessages(fread(cmd=paste("zcat -f ",file.path(pathtofiles, BSmetadata[x,4])," | awk '$4 || $5'"), stringsAsFactors=FALSE))
    #bedcoverageFile           <- suppressMessages(fread(cmd=paste("awk '$4 || $5'",file.path(pathtofiles, BSmetadata[x,4])), stringsAsFactors=FALSE))
    colnames(bedcoverageFile) <- c("chr", "start", "strand", "Cyt", "Thy", "di_context", "tri_context" )
    bedcoverageFile$label     <- rep(bedcoverageLabel, nrow(bedcoverageFile))
    
    return(bedcoverageFile)
    
  })
  #Bind list of DT
  study_DT    <- do.call(rbind, study_DT)
  
  
  cat("Listing and counting dinucleotides context : \n")
  print(table(study_DT$di_context))

  
  if(replicate){
    
    #Check for replicate
    merged_DT <- mergeDuplicateInDesign(BSmetadata, study_DT, stranded = TRUE)
    
  }else{
    
    return(study_DT)
    
  }  
}




#'mergeDuplicateInDesign
#'
#'Check for duplicate. If found they are merged by summing C and T.
#'This function is called in readBScoverage if the "replicate" parameter is set on TRUE
#'
#'@param BSmetadata path to the experimental metadata/design file following template
#'@param study_DT a dataTable that binds all coverage files by their labels
#'
#'
#'@export
#'
#'@return a list of Bismark coverage files
mergeDuplicateInDesign <- function(BSmetadata, study_DT, stranded = FALSE){
  
  if(length(which(duplicated(BSmetadata, by = c(1,2,3)))) > 0 ){
    
    ##Need to apply duplicated two times to find the index of duplicated lines  
    replicateIdx  <- which(duplicated(BSmetadata, by = c(1,2,3)))
    
    ##find which and how many labels are replicated
    dupLabels <- table(BSmetadata[replicateIdx, 1]) + 1
    
    cat("\nTechnical replicate detected : \n")
    cat(dupLabels, "\n\n" ,sep = "")
    cat("Merging counts between replicates\n\n")
    
    if(stranded){
      merged_study_DT <- study_DT[, .(Cyt = sum(Cyt), Thy = sum(Thy)), by = .(chr, start, label, strand)]
    }
    else{
      merged_study_DT <- study_DT[, .(Cyt = sum(Cyt), Thy = sum(Thy)), by = .(chr, start, label)]
    }
    return(merged_study_DT)
    
    
  } else {
    
    cat("No technical replicate detected in experimental design file\n\n")
    
    
  }
  
}





#'removeSNPwithVCF
#'
#'remove positions in study_DT for a given genome VCF file
#'
#'
#'@param VCF_path complete path to the VCF_file. Meaning it need the path to the file AND the name of the file
#'@param study_DT a dataTable that binds all coverage files by their labels
#'
#'
#'@export
#'
#'@return a dataTable object filtered from SNP
removeSNPwithVCF <- function(VCF_path, study_DT){
  
  #load VCF_file
  cat("Loading VCF file\n")
  #VCF_file     <- fread(file = VCF_path, skip = "#CHROM", header = T, col.names = c("chr", "pos", "id", "ref", "alt", "qual", "filter", "info"), select = (1:8))
  VCF_file     <- fread(file = VCF_path, skip = "#CHROM", header = T, col.names = c("chr", "start"), select = (1:2), stringsAsFactors=FALSE)
  ## transform to character column "chr" of studyDT
  study_DT$chr <- as.character(study_DT$chr)
  
  ## setkey to perform anti-join
  setkey(VCF_file, chr, start)
  setkey(study_DT, chr, start)
  
  ## Perform anti-join
  study_DT_SNP_filtered <- study_DT[!VCF_file]
  
  return(study_DT_SNP_filtered)
  
} 



#'removeSNPwithVCF_stranded
#'
#'remove positions in study_DT for a given genome VCF file
#'
#'
#'@param VCF_path complete path to the VCF_file. Meaning it need the path to the file AND the name of the file
#'@param study_DT a dataTable that binds all stranded meth call files by their labels
#'
#'
#'@export
#'
#'@return a dataTable object filtered from SNP
removeSNPwithVCF_stranded <- function(VCF_path, study_DT){
  
  #load VCF_file
  cat("Loading VCF file\n")
  #VCF_file     <- fread(file = VCF_path, skip = "#CHROM", header = T, col.names = c("chr", "pos", "id", "ref", "alt", "qual", "filter", "info"), select = (1:8))
  VCF_file     <- fread(file = VCF_path, skip = "#CHROM", header = T, col.names = c("chr", "start"), select = (1:2), stringsAsFactors=FALSE)
  ## transform to character column "chr" of studyDT
  study_DT$chr <- as.character(study_DT$chr)
  
  
  ##########FORWARD STRAND
  ## setkey to perform anti-join
  setkey(VCF_file, chr, start)
  setkey(study_DT, chr, start)
  
  ## Perform anti-join
  study_DT_SNP_filtered <- study_DT[!VCF_file]
  
  ##########REVERSE STRAND
  ## Add +1 to VCF to remove reverse strand snp
  VCF_file <- VCF_file[, start := start + 1]
  ## setkey to perform anti-join
  setkey(VCF_file, chr, start)
  
  ## Perform anti-join
  study_DT_SNP_filtered <- study_DT[!VCF_file]
  
  return(study_DT_SNP_filtered)
  
} 



#'mergePlusMinusPositions
#'
#'Add +1 to the cytosine position of minus strand
#'Aggregate by start position, doing the sum cytosine and thymine 
#'
#'
#'@param study_DT a dataTable that binds all coverage files by their labels, filtered from SNP positions
#'
#'
#'@export
#'
#'@return a dataTable of CpG counts free from strand
mergePlusMinusPositions <- function(study_DT){
  
  study_DT[strand == "-", start := start - 1]
  study_DT <- study_DT[, .(Cyt = sum(Cyt), Thy = sum(Thy)), by = .(chr, start, label)]
  return(study_DT)
}





#'BSFilterDepth
#'
#'Filter position of coverage table that fall under a min(depth) or are not covered by %(sample). Return the index of the rows filtered.
#'
#'The position removed are absolute to every sample
#'
#'@param minDepth Positions under this depth integer will be filtered from the table
#'@param sampleFraction Under this fraction of sample [between 0 and 1], position are filtered
#'@param study_DT a dataTable that binds all coverage files by their labels
#'
#'@export
#'
#'@return a vector of positions that fall under minDepth and sampleFraction
BSFilterMinDepth <- function(study_DT, minDepth = 0, sampleFraction = 0){
  
  ## get number of sample in table
  nbsample <- length(unique(study_DT$label))
  
  ## Remove position with no information
  study_DT  <- study_DT[Cyt != 0 | Thy != 0]
  
  ## create column counts
  study_DT[, count := Cyt + Thy]
  
  # create boolean column on count
  study_DT[, sup_th := count >= minDepth]
  
  ## count and compute fraction of position in samples
  study_DT[, fract := sum(sup_th)/nbsample, by = c("start", "chr")]
  
  ## Make list of position to remove
  ## no need to return strand inf because forward and reverse can't share positions in the table
  pos_chr_under_minCov <- unique(study_DT[fract <= sampleFraction][, c("start", "chr")])
  
  return(pos_chr_under_minCov)
  
  
} 

#'BSFilterMaxDepth
#'
#'Filter position of coverage table that fall under a min(depth) or are not covered by %(sample). Return the index of the rows filtered.
#'The position removed are absolute to every sample 
#'
#'@param maxdepth Under this fraction of sample [between 0 and 1], position are filtered
#'@param study_DT a dataTable that binds all coverage files by their labels
#'
#'@export
#'
#'@return a vector of positions that fall above maxDepth parameter 
BSFilterMaxDepth <- function(study_DT, maxDepth = 0){
  
  ## Remove position with no information
  #study_DT[Cyt != 0 | Thy != 0]
  
  ## create column counts
  study_DT[, count := Cyt + Thy]

  ## Make list of position to remove
  ## no need to return strand inf because forward and reverse can't share positions in the table
  pos_chr_maxDepth <- unique(study_DT[count > maxDepth ][, c("start", "chr")])
  
  return(pos_chr_maxDepth)
  
  
} 

#'BSCoverageNorm
#'
#'Apply a median normalisation on Total counts, Cytosine counts, Thymine counts
#'
#'@param study_DT a dataTable that binds all coverage files by their labels
#'@param minCov output of BSFilterDepth with positions to be removed. If provided the positions will be removed from study_DT before normalisation.
#'@param maxCov output of BSFilterMaxDepth with positions to be removed. If provided the positions will be removed from study_DT before normalisation.
#'
#'@export
#'
#'@return a vector of positions that fall under 
BSCoverageNorm <- function(study_DT, minCov=NULL, maxCov=NULL){
  
  ## filter non-covered value - if we don't we might have some forbidden operation involving two 0
  study_DT_allcovered <- study_DT[Cyt != 0 | Thy != 0]
  
  if(!missing(minCov)){
    
    study_DT_allcovered <- study_DT_allcovered[!minCov]
    
  }
  
  if(!missing(maxCov)){
    
    study_DT_allcovered <- study_DT_allcovered[!maxCov]
    
  }
  
  ## add column counts
  study_DT_allcovered[, counts := Cyt + Thy] 
  
  ## add column median of counts computed per label
  study_DT_allcovered[, med := median(as.numeric(counts)), by = label]
  
  ## add scaling factor
  study_DT_allcovered[, sc.fact := med / max(med)]
  
  ## Updating Cyt and Thy by dividing them by counts
  study_DT_allcovered[, `:=`(Cyt = Cyt / counts , Thy = Thy / counts)]
  
  ## each coverage divided by its scaling factor
  study_DT_allcovered[, counts := counts / sc.fact]

  ##Update Cyt and Thy again by doing the product of scaled coverage with each column
  study_DT_allcovered[, `:=`(Cyt = Cyt * counts, Thy = Thy * counts)][, `:=`(Cyt = round(Cyt), Thy = round(Thy))]
  
  study_DT_allcovered[, count := Cyt + Thy]

  return(study_DT_allcovered[, c(1:7)])

  
} 



#'makeBStablePerSample
#'
#'From the whole study dataTable make one table per sample and write it to a tsv file. Files are stores in mainDir/Bismark_format/*_sample.tsv
#'
#'@param study_DT a dataTable that binds all coverage files by their labels
#'
#'
#'@export
#'
#'@return a list of dataTables (one for each label) 
makeBStablePerSample <- function(study_DT){
  
  ## Produce list of data.table for each samples(= labels)
  listed_format_table <- split(study_DT, by ="label")
  
  ## Keep only needed column for DSS
  listed_format_table <- lapply(listed_format_table, function(x){
    
    x <- x[, c(1,2,7,5,4)]
    colnames(x) <- c("chr", "pos", "N", "X", "strand")
    return(x)
    
  })
  
}

#'writeBStables
#'
#'Write the list of BS format dataTable into files at given outputDir
#'
#'@param listBSDT  a list of processed tables
#'@param outputDir Path to the complete directory where to write files
#'
#'
#'@export
#'
#'@return blank
writeBStables <- function(listBSDT, outputDir){
  
  # Create directory
  dir.create(file.path(outputDir,"Bismark_format"), recursive = TRUE)
  
  # Write 
  invisible(lapply(names(listBSDT), function(sampleName){
    
    if(!file.exists(file.path(outputDir,"Bismark_format", paste(sampleName, "_bismark.tsv", sep="")))){
      cat(paste("writing table for :", sampleName, end = "\n"))
      
      fwrite(x = listBSDT[[sampleName]], file = file.path(outputDir,"Bismark_format", paste(sampleName, "_bismark.tsv", sep = "")), sep = "\t")
      
    }else{
      cat(paste(sampleName, "_bismark.tsv", "already exists in folder, overwriting", end = "\n"))
      fwrite(x = listBSDT[[sampleName]], file = file.path(outputDir,"Bismark_format", paste(sampleName, "_bismark.tsv", sep = "")), sep = "\t")
    }
  }))
}