---
title: "Bismark_coverage_preprocessing"
author: "P_Terzian"
date: "03/12/2019"
output:
  prettydoc::html_pretty:
  theme: architect
  toc: true
---

## Ce document est une feuille de route pour le développement et l'utilisation des fonctions stockées dans BS_process_functions.R


### Load module & functions

```{r load module, message=FALSE}
library(data.table)
library(Biostrings)


source('~/Documents/axis2-epigenectics/script_BSseq/BS_process_functions.R')

```

__should I use library() or require() ?__


These command generate reproducible example while building functions
```{r Test data}

BigTable_test <- data.table(chr=c(1,1,1,2,2,3,3,4), start = c(25,26,26,101,101,25,25,26), stop = c(25,26,26,101,101,25,25,26), fraction = c(100,0,90,70,100,0,100, 90) , C = c(0,2,3,1,10,16,5,6), T = c(1,7,6,5,2,3,10,1), lab = c(rep("A1", 5), rep("A2", 3)))

BigTable_test <- data.table(start = c(25,26,101,25,26,100), lab = c(rep("A1", 3), rep("A2", 3)))

test <- data.table(start = c(100,100,101,101,103,103,105,105,107,107), id = c("jj", "jp","jj", "jp", "jj", "jp", "jj", "jp", "jj", "jp"), counts = c(0,0,7,15,6,7,9,1,3,4))


##Biostring test in loop

huTest <- readDNAStringSet("http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr21_GL383578v2_alt.fa.gz")

sapply(names(huTest), function(x){
  
  print(x)
  print(huTest$x)
  print(huTest[[x]][200])
  
})

```



Load metadata table :

```{r metadata}
pathToMetadata  <- "../../WGBS/template_metadata.tsv"
Test_BSmetadata      <- fread(file = pathToMetadata, header = T)
#setKey
setkey(Test_BSmetadata)

```


### Step1 : Read Bismark Coverage file
Step1 : Detect and sum technical replicate

I implemented the third step into the second step.
```{r read coverage file}
##PATH to coverage file switched to stranded_report
# pathToFiles <- "../../WGBS/BSseq-wgbs-test/results/bismark_methylation_calls/methylation_coverage/*"
# step1_wholeTable <- readBScoverage(BSmetadata = Test_BSmetadata, pathtofiles = pathTofiles, replicate = T)

pathToFiles <- "../../WGBS/BSseq-wgbs-test/results/bismark_methylation_calls/stranded_CpG_report/"
step1_wholeTable <- readBScoverage_stranded(BSmetadata = Test_BSmetadata, pathtofiles = pathToFiles, replicate = T)
 
```

__One issue here : When there is more than 2 duplicate, the number of total file is N+1 instead of N. Need to check what the table() is getting__


```{r verifying stranded output}
#load chr21 fasta sequence of gallus gallus
chr21_ref <- readDNAStringSet("../../WGBS/BSseq-wgbs-test/chr21.fa")
#Find number of CpG in sequence * 2 (both strand)
nrow(as.data.frame(vmatchPattern( "CG",chr21_ref))) * 2 
#Find number of position shown in stranded_mehylation_called file per samples 
table(step1_wholeTable$label)

## We can see all CpG positions of chromosome 21 are listed in bismark stranded outputs
```



### step 2 : Remove known snp from the table
```{r remove VCF}

filtered_merged_table_stranded <- removeSNPwithVCF(VCF_path = "../../WGBS/Gallus_gallus.vcf", study_DT = step1_wholeTable)

```


These are the full col.names of the original VCF_file c("chr", "pos", "id", "ref", "alt", "qual", "filter", "info")
__SHOULD WE LOAD ALL COLUMN ? or only the two first ?__

### step 3 : Remove non CpG position (Automatically done by bismark)





### Step 4 : Make List of position to filter with minCov and sampleFract parameters
```{r list position to filterminSample}

# #create column counts
# filtered_merged_table_stranded[, count := Cyt + Thy]
# #get number of sample
# nbsample <- length(unique(filtered_merged_table_stranded$label))
# #count nb of each position per chromsome
# filtered_merged_table_stranded[, freq := .N, by = c("start", "chr")]
# # Compute fraction 
# filtered_merged_table_stranded[, freq := freq/nbsample]
# # Make list of position to remove
# unique(filtered_merged_table_stranded[count >! 3 & freq >! 0.75][, c("start", "chr")])

pos_to_remove_minDepth <- BSFilterMinDepth(study_DT = filtered_merged_table_stranded, minDepth = 2, sampleFraction = 0.4)

```

### Step 5 : Make List of position with high depth
```{r list position to filterMinSample}

pos_to_remove_maxDepth <- BSFilterMaxDepth(study_DT = filtered_merged_table_stranded, maxDepth = 200)

```

### Step 6 : Median normalisation
```{r}

# ## filter non-covered value
# no0_filtered_merged_stranded <- filtered_merged_table_stranded[Cyt !=0 & Thy != 0]
# 
# ## add column counts
# no0_filtered_merged_stranded[, counts := Cyt + Thy] 
# 
# ## add column median of counts computed per label
# no0_filtered_merged_stranded[, med := median(as.numeric(counts)), by = label]
# 
# ## check unique median
# #no0_filtered_merged_stranded[, unique(med)]
# 
# ## add scaling factor
# no0_filtered_merged_stranded[, sc.fact := med / max(med)]
# 
# ## Updating Cyt and Thy by dividing them by counts
# no0_filtered_merged_stranded[, `:=`(Cyt = Cyt / counts , Thy = Thy / counts)]
# 
# ## each coverage divided by its scaling factor
# no0_filtered_merged_stranded[, counts := counts * sc.fact]
# 
# ##Update Cyt and Thy again by doing the product of scaled coverage with each column. Finally round these products
# no0_filtered_merged_stranded[, `:=`(Cyt = Cyt * counts, Thy = Thy * counts)][, `:=`(Cyt = round(Cyt), Thy = round(Thy))]

norm_merged_table <- BSCoverageNorm(study_DT = filtered_merged_table_stranded)

norm_merged_table <- BSCoverageNorm(study_DT = filtered_merged_table_stranded, minCov = pos_to_remove_minDepth, maxCov = pos_to_remove_maxDepth)



```

Question sur la normalisation par la médiane. Je calcul la médiane sur l'ensemble des valeurs, strand confondu, position non couverte ou non ?

Je commence par retirer les valeurs manquantes du coup. On se posera ensuite la question de retirer le max et min coverage pour la normalisation ou non.


### Step 7 : Remove positions listed in 3 and 4
```{r list position to filterMaxSample}

##filter table with position listed in step 4 
norm_merged_table <- norm_merged_table[!pos_to_remove_minDepth]

##filter table with position listed in step 5
norm_merged_table <- norm_merged_table[!pos_to_remove_maxDepth]


```


### Split the big table to make one table per sample & write then into table
```{r}


listofDT <- makeBStablePerSample(norm_merged_table)

writeBStables(listofDT, "../../WGBS/TEST_preprocess")

# 
# dir.create(file.path("../../WGBS/TEST_preprocess","Bismark_format"))
# ## Produce list of data.table by label
# listed_format_table <- split(norm_merged_table, by ="label")
# 
# ## Keep only needed column for DSS
# listed_format_table <- lapply(listed_format_table, function(x){
# 
#   x <- x[, c(1,2,7,5,4)]
#   colnames(x) <- c("chr", "pos", "N", "X", "strand")
#   return(x)
#   
# })
#   
# ## Then write all tables in directory
# lapply(names(listed_format_table), function(x){
# 
#   if(!file.exists(file.path("../../WGBS/TEST_preprocess","Bismark_format",paste(x, "_bismark.tsv", sep="")))){
#     
#     fwrite(x = listed_format_table[[x]], file = file.path("../../WGBS/TEST_preprocess","Bismark_format",paste(x, "_bismark.tsv", sep = "")), sep = "\t")
#              
#   }else{
#     cat(paste(x, "_bismark.tsv", "already exists in folder"))
#   }
# })





```

